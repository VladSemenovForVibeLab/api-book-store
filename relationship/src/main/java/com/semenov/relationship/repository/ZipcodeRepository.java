package com.semenov.relationship.repository;

import com.semenov.relationship.model.Zipcode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZipcodeRepository extends CrudRepository<Zipcode,Long> {

}
