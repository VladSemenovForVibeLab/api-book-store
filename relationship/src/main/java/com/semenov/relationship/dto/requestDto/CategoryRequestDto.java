package com.semenov.relationship.dto.requestDto;

import lombok.Data;

@Data
public class CategoryRequestDto {
    private String name;
}
