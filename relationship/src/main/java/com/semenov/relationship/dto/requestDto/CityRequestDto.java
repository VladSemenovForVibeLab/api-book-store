package com.semenov.relationship.dto.requestDto;

import lombok.Data;

@Data
public class CityRequestDto {
    private String name;
}
