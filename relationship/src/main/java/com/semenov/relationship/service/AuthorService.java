package com.semenov.relationship.service;

import com.semenov.relationship.dto.requestDto.AuthorRequestDto;
import com.semenov.relationship.dto.responseDto.AuthorResponseDto;
import com.semenov.relationship.model.Author;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AuthorService {
    public AuthorResponseDto addAuthor(AuthorRequestDto authorRequestDto);
    List<AuthorResponseDto> getAuthors();

    AuthorResponseDto getAuthorById(Long authorId);
    Author getAuthor(Long authorId);

    AuthorResponseDto deleteAuthor(Long authorId);
    AuthorResponseDto editAuthor(Long authorId, AuthorRequestDto authorRequestDto);
    AuthorResponseDto addZipcodeToAuthor(Long authorId,Long zipcodeId);
    AuthorResponseDto deleteZipcodeFromAuthor(Long authorId);
}
