package com.semenov.relationship.service;

import com.semenov.relationship.dto.requestDto.CityRequestDto;
import com.semenov.relationship.model.City;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CityService {
    City addCity(CityRequestDto cityRequestDto);
    List<City> getCities();
    City getCity(Long cityId);
    City deleteCity(Long cityId);
    City editCity(Long cityId,CityRequestDto cityRequestDto);
}
