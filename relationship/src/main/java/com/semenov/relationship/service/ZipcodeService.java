package com.semenov.relationship.service;

import com.semenov.relationship.dto.requestDto.ZipcodeRequestDto;
import com.semenov.relationship.model.Zipcode;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ZipcodeService {
    Zipcode addZipcode(ZipcodeRequestDto zipcodeRequestDto);

    List<Zipcode> getZipcodes();

     Zipcode getZipcode(Long zipcodeId);

     Zipcode deleteZipcode(Long zipcodeId);

     Zipcode editZipcode(Long zipcodeId,ZipcodeRequestDto zipcodeRequestDto);

     Zipcode addCityToZipcode(Long zipcodeId,Long cityId);

     Zipcode removeCityFromZipcode(Long zipcodeId);
}
