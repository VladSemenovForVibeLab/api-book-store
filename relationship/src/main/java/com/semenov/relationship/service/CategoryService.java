package com.semenov.relationship.service;

import com.semenov.relationship.dto.requestDto.CategoryRequestDto;
import com.semenov.relationship.dto.responseDto.CategoryResponseDto;
import com.semenov.relationship.model.Category;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {
    Category getCategory(Long categoryId);

    CategoryResponseDto addCategory(CategoryRequestDto categoryRequestDto);
    CategoryResponseDto getCategoryById(Long categoryId);
    List<CategoryResponseDto> getCategories();
    CategoryResponseDto deleteCategory(Long categoryId);
    CategoryResponseDto editCategory(Long categoryId, CategoryRequestDto categoryRequestDto);

}
