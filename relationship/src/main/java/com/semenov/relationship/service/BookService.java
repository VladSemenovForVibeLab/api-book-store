package com.semenov.relationship.service;

import com.semenov.relationship.dto.requestDto.BookRequestDto;
import com.semenov.relationship.dto.responseDto.BookResponseDto;
import com.semenov.relationship.model.Book;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {
    BookResponseDto addBook(BookRequestDto bookRequestDto);
    BookResponseDto getBookById(Long bookId);
    Book getBook(Long bookId);
    List<BookResponseDto> getBooks();
    BookResponseDto deleteBook(Long bookId);
    BookResponseDto editBook(Long bookId,BookRequestDto bookRequestDto);
    BookResponseDto addAuthorToBook(Long bookId, Long authorId);
    BookResponseDto deleteAuthorFromBook(Long bookId,Long authorId);
    BookResponseDto addCategoryToBook(Long bookId,Long categoryId);
    BookResponseDto removeCategoryFromBook(Long bookId,Long categoryId);
}
