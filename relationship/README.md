# Магазин книг

Проект представляет собой серверную часть магазина книг, реализованную на Java 8 с использованием Spring Boot 2.5.6.

## Зависимости

- Java 8
- Spring Boot 2.5.6
- Springfox 3.0.0
- starter jpa
- starter web
- h2
- lombok
- maven

## Установка

---

Для запуска проекта требуется установить следующие зависимости:

- Java 8
- Maven

---

1. Клонируйте репозиторий с помощью команды:

   ```shell
   git clone git@gitlab.com:VladSemenovForVibeLab/api-book-store.git
   ```

2. Перейдите в директорию проекта:

   ```shell
   cd relationship
   ```

3. Соберите проект с помощью Maven:

   ```shell
   mvn install
   ```

4. Запустите проект:

   ```shell
   java -jar target/your-project.jar
   ```

### Доступные эндпоинты примеры запросов можно посмотерть по адресу 

```
http://localhost:8080/swagger-ui/index.html#/
```